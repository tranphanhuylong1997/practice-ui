import {all, call, fork, put, takeLatest} from 'redux-saga/effects'
import rsf from '../../firebase/index';
import {myFirebaseApp} from '../../firebase/index';
import {
    PUSH_LOGIN_WITH_EMAIL,
    SET_LOGIN_SUCCESS,
    GET_PROFILE,
    GET_PROFILE_SUCCESS, SET_LOGOUT_SUCCESS, PUSH_LOGOUT, SET_LOADING_POPUP, SET_LOGIN_FAILURE
} from "../../constant/actionTypes";

function* watchGetProfile() {
    yield takeLatest(GET_PROFILE, function* (action) {
        try {
            const profile = myFirebaseApp.auth().currentUser;
            if (profile) {
                yield put({
                    type: GET_PROFILE_SUCCESS,
                    payload: {
                        profile: profile.email,
                    }
                })
            }
        } catch (error) {
            console.log(error)
        }
    });
}

function* watchLogin() {
    yield takeLatest(PUSH_LOGIN_WITH_EMAIL, function* (action) {
        const email = action.payload.email;
        const password = action.payload.password;
        try {
            yield put({
                type: SET_LOADING_POPUP,
                payload: {
                    isLoadingPopup: true
                }
            });
            const data = yield call(rsf.auth.signInWithEmailAndPassword, email, password);
            
            yield put({
                type: SET_LOGIN_SUCCESS,
                payload: {
                    isLoginSuccess: true
                }
            });
            yield put({
                type: GET_PROFILE,
            });

        } catch (error) {
            yield put({
                type: SET_LOGIN_FAILURE,
                payload: {
                    error: 'Invalid email or password'
                }
            })
        }
        yield put({
            type: SET_LOADING_POPUP,
            payload: {
                isLoadingPopup: false
            }
        });
    });
}

function* watchLogout() {
    yield takeLatest(PUSH_LOGOUT, function* (action) {

        yield put({
            type: SET_LOADING_POPUP,
            payload: {
                isLoadingPopup: true
            }
        })
        yield call(rsf.auth.signOut)
        // successful logout will trigger the loginStatusWatcher, which will update the state
        yield put({
            type: SET_LOGOUT_SUCCESS,
            payload: {
                isLogoutSuccess: true,
            }
        });
        yield put({
            type: SET_LOADING_POPUP,
            payload: {
                isLoadingPopup: false
            }
        });
    })
}

export default function* loginRootSaga() {
    yield all([
        fork(watchLogin),
        fork(watchLogout),
        fork(watchGetProfile),
    ])
}
