import {
    GET_PROFILE_SUCCESS,
    SET_LOGIN_SUCCESS,
    SET_LOGOUT_SUCCESS,
    SET_LOGIN_FAILURE
} from "../../constant/actionTypes";

const initialState = {
    isLogoutSuccess: false,
    isLoginSuccess: false,
    profile: null,
    error: null,
}

export default function loginReducer(state = initialState, action) {
    switch (action.type) {
        case SET_LOGIN_SUCCESS:
            return {
                ...state,
                isLoginSuccess: action.payload.isLoginSuccess
            }
        case SET_LOGIN_FAILURE:
            return {
                ...state,
                error: action.payload.error
            }
        case SET_LOGOUT_SUCCESS:
            return {
                ...state,
                isLogoutSuccess: action.payload.isLogoutSuccess,
            }
        case GET_PROFILE_SUCCESS:
            return {
                ...state,
                profile: action.payload.profile,
            };
        default:
            return state
    }
}