import {
    PUSH_LOGIN_WITH_EMAIL,
    SET_LOGIN_SUCCESS,
    PUSH_LOGOUT,
    SET_LOGOUT_SUCCESS,
    GET_PROFILE, SET_LOGIN_FAILURE
} from "../../constant/actionTypes";

export const pushLogin = (email, password) => ({
    type: PUSH_LOGIN_WITH_EMAIL,
    payload: {
        email, password
    }
})
export const pushLoginFailure = () => ({
    type: SET_LOGIN_FAILURE,
    payload: {
        error
    }
})
export const pushLoginSuccess = () => ({
    type: SET_LOGIN_SUCCESS,
    payload: {
        isLoginSuccess: false,
    }
})

export const pushLogout = () => ({
    type: PUSH_LOGOUT,
})

export const pushLogoutSuccess = () => ({
    type: SET_LOGOUT_SUCCESS,
    payload: {
        isLogoutSuccess: false,
    }
})

export const getProfile = () => {
    return {
        type: GET_PROFILE,
    };
};
