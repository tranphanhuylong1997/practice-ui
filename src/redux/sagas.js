import {all} from 'redux-saga/effects';
import authSaga from './auth/saga';
import loadingSaga from './loading/saga';

export default function* rootSaga() {
    yield all([authSaga(), loadingSaga()]);
}