import {combineReducers} from "redux";
import auth from '../redux/auth/reducer';
import loading from '../redux/loading/reducer';
import {firebaseReducer} from 'react-redux-firebase';

const reducers = combineReducers({
    auth,
    loading,
    firebase: firebaseReducer
});
export default reducers;