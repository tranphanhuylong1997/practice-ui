import createSagaMiddleware from 'redux-saga';
import {applyMiddleware, compose, createStore} from 'redux';

import reducers from './reducer';
import saga from './sagas';
import '../firebase/index';

const sagaMiddleware = createSagaMiddleware();

export function configureStore() {
    const store = createStore(reducers, applyMiddleware(sagaMiddleware));
    sagaMiddleware.run(saga);
    return store;
};
