import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import Started from '../component/screen/StartScreen';
import LoginScreen from '../component/screen/LoginScreen';
import ProfileScreen from "../component/screen/ProfileScreen";

const Stack = createStackNavigator();

const Navigation = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="start" component={Started} options={{headerShown: false}}/>
                <Stack.Screen name='login' component={LoginScreen} options={{headerShown: false}}/>
                <Stack.Screen name='profile' component={ProfileScreen} options={{headerShown: false}}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}
export default Navigation;