import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container: {
        marginTop: 5,
    },
    button: {
        backgroundColor: '#0086b3',
    },
    text: {
        fontSize: 22,
        color: 'white',
        textAlign: 'center',
        padding: 10,
    }
});
export default styles;