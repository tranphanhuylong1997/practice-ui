import React from 'react';
import {Text, TouchableOpacity, View} from "react-native";

import styles from './styles';

const LogoutButton = ({title, onPress}) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.button} onPress={onPress}>
                <Text style={styles.text}>{title}</Text>
            </TouchableOpacity>
        </View>
    )
};
export default LogoutButton;