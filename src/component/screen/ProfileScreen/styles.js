import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        width: '100%',
        backgroundColor: '#001a33',
        justifyContent: 'center',
        alignItems: 'center',
    },
    button: {
        backgroundColor: '#0086b3',
    },
    content: {
        width: '100%',
        paddingHorizontal: 42
    },
    username: {
        textAlign: 'center',
        fontSize: 22,
        color: 'white'
    }
});
export default styles;