import React, {useEffect} from 'react';
import {Alert, Text, View} from "react-native";
import {useDispatch, useSelector} from "react-redux";

import LogoutButton from "./LogoutButton";
import {pushLogout, pushLogoutSuccess} from "../../../redux/auth/action";

import styles from './styles';

const ProfileScreen = ({navigation}) => {
    const dispatch = useDispatch();
    const profile = useSelector((state) => state.auth.profile);
    const isLogoutSuccess = useSelector((state) => state.auth.isLogoutSuccess);

    function handleLogoutOnPress() {
        Alert.alert(
            '',
            'Are you sure ?',
            [
                {
                    text: 'Cancel',
                    onPress: () => null,
                    style: 'cancel',
                },
                {text: 'Yes', onPress: handlePushLogoutOnPress},
            ],
            {cancelable: false}
        );
    }

    function handlePushLogoutOnPress() {
        dispatch(pushLogout());
    }

    useEffect(() => {
        if (isLogoutSuccess === true) {
            navigation.navigate('start');
            dispatch(pushLogoutSuccess());
        }
    }, [isLogoutSuccess]);

    return (
        <View style={styles.container}>
            <View style={styles.content}>
                <Text style={styles.username}>Hi,{profile}</Text>
                <LogoutButton title='Logout' onPress={handleLogoutOnPress}/>
            </View>

        </View>
    )
}
export default ProfileScreen;