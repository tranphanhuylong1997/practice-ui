import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: 15,
        paddingHorizontal: 20,
    },
    text: {
        fontSize: 28,
        color: 'white',
        textAlign: 'center',
    }
});
export default styles;