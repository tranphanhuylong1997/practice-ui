import React from 'react';
import { Text } from 'react-native';

import styles from './styles';


const ErrorMessageText = ({ message }) => {

    if (message) {
        return <Text style={styles.text}>{message}</Text>;
    }
    return null;
};
export default ErrorMessageText;
