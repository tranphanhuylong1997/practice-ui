import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({
    text: {
        width: '100%',
        fontSize: 14,
        color: 'red',
        paddingHorizontal: 5,
    },
});

export default styles;
