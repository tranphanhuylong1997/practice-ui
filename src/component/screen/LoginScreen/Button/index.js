import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

import styles from './styles';

const CustomButton = ({ title, onPress }) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.button} onPress={onPress}>
                <Text style={styles.title}>{title}</Text>
            </TouchableOpacity>
        </View>
    )
}
export default CustomButton;