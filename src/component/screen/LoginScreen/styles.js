import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        width: '100%',
        backgroundColor: '#001a33',
        paddingHorizontal: 42,
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    content: {
        flexDirection: 'column',
        width: '100%',
        marginTop: 80,
    },
    formLogin: {
        width: '100%',
        flexDirection: 'column',
        marginBottom: 50
    },
    backIcon: {
        marginLeft: -10,
        fontSize: 25
    },
    separated1: {
        height: 30,
    },
    separated2: {
        height: 20,
    },
});
export default styles;