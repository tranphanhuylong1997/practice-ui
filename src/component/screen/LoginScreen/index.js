import React, {useEffect} from 'react';
import {KeyboardAvoidingView, Platform, View, Text} from 'react-native';
import {Ionicons as BackIcon} from '@expo/vector-icons';
import {useDispatch, useSelector} from "react-redux";
import {useFormik} from 'formik';
import * as Yup from 'yup';

import Title from './Title';
import LoginInput from './InputForm';
import CustomButton from './Button';

import {pushLogin, pushLoginSuccess} from "../../../redux/auth/action";
import styles from './styles';
import {setLoadingPopup} from "../../../redux/loading/action";

const LoginScreen = ({navigation}) => {
    const dispatch = useDispatch();
    const isLoginSuccess = useSelector((state) => state.auth.isLoginSuccess);
    const error = useSelector((state) => state.auth.error);
    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
        },
        validationSchema: Yup.object().shape({
            email: Yup.string().required('Email is required').email('Email invalid'),
            password: Yup.string()
                .required('Password is required')
                .min(6, 'Password must have at least 6 characters')
                .max(20, 'Password must be less than 20 characters'),
        }),
        onSubmit: handleLoginOnPress
        // values => alert(`Email: ${values.email}, Password: ${values.password}`),
    });

    function handleLoginOnPress(values) {
        const email = values.email;
        const password = values.password;
        dispatch(pushLogin(email, password));
    }

    function handleGoBackOnPress() {
        navigation.goBack();
    }

    useEffect(() => {
        if (isLoginSuccess === true) {
            navigation.replace('profile');
            dispatch(pushLoginSuccess());
        }
    }, [isLoginSuccess]);

    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : null}
            enabled
            keyboardVerticalOffset={Platform.OS === 'ios' ? 30 : 0}
            style={styles.container}>

            <View style={styles.content}>
                <BackIcon name="chevron-back-outline" size={34} color="white" onPress={handleGoBackOnPress}
                          style={styles.backIcon}>Back</BackIcon>
                <Title text="Sign in to Chopet.vn and continue"/>
            </View>
            <View style={styles.formLogin}>
                <LoginInput
                    autoCapitalize="none"
                    icon='email'
                    keyboardType='email-address'
                    onBlur={formik.handleBlur('email')}
                    onChangeText={formik.handleChange('email')}
                    value={formik.values.email}
                    errorMsg={error || formik.errors.email}
                    placeholder='Username'/>
                <LoginInput
                    autoCapitalize="none"
                    icon='vpn-key'
                    keyboardType='default'
                    secureTextEntry
                    onBlur={formik.handleBlur('password')}
                    onChangeText={formik.handleChange('password')}
                    value={formik.values.password}
                    errorMsg={error || formik.errors.password}
                    placeholder='Password'/>
                <CustomButton title='Login' onPress={formik.handleSubmit}/>
            </View>
        </KeyboardAvoidingView>
    )
};
export default LoginScreen;