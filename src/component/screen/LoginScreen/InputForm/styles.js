import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'column',
        marginBottom: 15,
    },
    form: {
        width: '100%',
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#00ccff',
        backgroundColor: '#00284d',
        alignItems: 'center',
        paddingHorizontal: 10,
    },
    textInput: {
        width: '100%',
        height: 50,
        color: '#FFFFFF',
        fontSize: 22,
        paddingVertical: 5,
        paddingHorizontal: 20,
        alignItems: 'center',
    },
    icon: {
        padding: 10,
    }
});

export default styles;
