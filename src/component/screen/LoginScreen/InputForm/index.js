import React from 'react';
import {View, TextInput} from 'react-native';
import {MaterialIcons as Icon} from '@expo/vector-icons';

import ErrorMessageText from '../ErrorMessageText';

import styles from './styles';

const InputForm = ({placeholder, onChangeText, value, errors, errorMsg, touched, icon, ...props}) => {
    return (
        <View style={styles.container}>
            <View style={styles.form}>
                <Icon name={icon} size={25} color="white"/>
                <TextInput
                    style={styles.textInput}
                    placeholder={placeholder}
                    placeholderTextColor="#fff"
                    onChangeText={onChangeText}
                    value={value}
                    errors={errors}
                    touched={touched}
                    {...props}
                />
            </View>
            <ErrorMessageText message={errorMsg}/>
        </View>
    )
}
export default InputForm;