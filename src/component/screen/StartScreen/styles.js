import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        width: '100%',
        backgroundColor: '#001a33',
        paddingHorizontal: 48,
        justifyContent: 'center',
    },
});
export default styles;