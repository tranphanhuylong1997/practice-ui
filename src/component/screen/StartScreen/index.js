import React, {useEffect} from 'react';
import {useDispatch} from "react-redux";
import {View} from 'react-native';

import CustomButton from '../LoginScreen/Button';
import Title from '../LoginScreen/Title';

import styles from './styles';

import {setLoadingPopup} from "../../../redux/loading/action";


const Started = ({navigation}) => {
    const dispatch = useDispatch();

    function handleOnPressStarted() {
        navigation.navigate('login');
    }

    useEffect(() => {
        dispatch(setLoadingPopup(false));
    }, [])
    return (
        <View style={styles.container}>
            <Title text="Welcome To Chopet.vn"/>
            <CustomButton title="Get Started" onPress={handleOnPressStarted}/>
        </View>
    )
};
export default Started;