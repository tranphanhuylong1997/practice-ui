import React from 'react';

import LoadingHOC from './src/HOC/loading';

import {Provider} from "react-redux";

import Navigation from './src/navigation/index';

import {configureStore} from './src/redux/store';

export default function App() {
    return (
        <Provider store={configureStore()}>
            <LoadingHOC>
                <Navigation/>
            </LoadingHOC>
        </Provider>
    );
};

